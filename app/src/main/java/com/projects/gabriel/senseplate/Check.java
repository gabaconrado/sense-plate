package com.projects.gabriel.senseplate;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.projects.gabriel.senseplate.database.Food_Meal;
import com.projects.gabriel.senseplate.database.Meal;
import com.projects.gabriel.senseplate.database.SensePlateDBHelper;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Check extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getResources().getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getResources().getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getResources().getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.check, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_check, container, false);

            //create the view
            TextView txtDailyInfo = (TextView) rootView.findViewById(R.id.txtAllInformation);
            ListView lvAllMeals = (ListView) rootView.findViewById(R.id.lvAllMeals);

            int selection = getArguments().getInt(ARG_SECTION_NUMBER);

            SensePlateDBHelper dbHelper = new SensePlateDBHelper(getActivity());


            Date date = new Date();
            List<Meal> allMeals;
            SimpleDateFormat dateFormat;

            switch(selection){
                case 1:
                    dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    allMeals = dbHelper.getAllMeal(dateFormat.format(date));
                    break;
                case 2:
                    dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    allMeals = dbHelper.getAllMealWeek(dateFormat.format(date));
                    break;
                case 3:
                    dateFormat = new SimpleDateFormat("yyyy-MM");
                    allMeals = dbHelper.getAllMeal(dateFormat.format(date));
                    break;
                default:
                    allMeals = new ArrayList<>();
            }

            dbHelper.close();
            String[] meal_desc = new String[allMeals.size()];

            for(int i = 0; i < allMeals.size(); i++){

                meal_desc[i] = allMeals.get(i).getDescription();

            }
            DecimalFormat df = new DecimalFormat("#.00");
            txtDailyInfo.setText("Total food consumed = " + df.format(sumWeightAllMeals(allMeals)) + "g");
            ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, meal_desc);
            lvAllMeals.setAdapter(adapter);
            return rootView;
        }



        public Double sumWeightAllMeals(List<Meal> meals){

            Double sum = 0.0;

            SensePlateDBHelper dbHelper = new SensePlateDBHelper(getActivity());

            for(Meal m : meals){

                sum += sumWeightMeal(dbHelper.food_mealFromMeal(m));

            }

            dbHelper.close();
            return sum;

        }

        public Double sumWeightMeal(List<Food_Meal> food_meals){

            Double sum = 0.0;

            for(Food_Meal fm : food_meals){

                sum += fm.getFoodWeight();

            }

            return sum;

        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((Check) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
