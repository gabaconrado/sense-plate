package com.projects.gabriel.senseplate;


import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.projects.gabriel.senseplate.database.Food;
import com.projects.gabriel.senseplate.database.SensePlateDBHelper;
import com.projects.gabriel.senseplate.util.CreateMealDialog;
import com.projects.gabriel.senseplate.util.SensePlateArrayAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;


public class Eat extends ActionBarActivity implements CreateMealDialog.MealDialogListener {

    public void makeToast(String s){

        Toast t = Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG);
        t.show();

    }
    /*********************bluetoooth*******************************************/
    private BluetoothAdapter mBtAdapter;
    private BluetoothServerSocket mBtServer;
    private BluetoothSocket mBtSocket;
    private InputStream mInputStream;
    private boolean isConnected = false;

    private final UUID myUUID = UUID.fromString("03dddf03-1f70-4435-8433-de1d59acefb6");
    private final String name_service = "SensePlateBTServer";

    public void setupBluetooth(){
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBtAdapter == null)
            //does not support
            makeToast("Device does not support Bluetooth");
        else{
            Intent discoverableIntent = new
            Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 100);
            startActivity(discoverableIntent);
        }
    }

    public BluetoothSocket setupServer() throws IOException {

        makeToast("Trying to connect");
        mBtServer = mBtAdapter.listenUsingInsecureRfcommWithServiceRecord(name_service, myUUID);
        BluetoothSocket btSocket = mBtServer.accept();
        mBtServer.close();
        if(btSocket == null)
            makeToast("Server setup non-successfully");
        else {
            makeToast("Connected successfully.");
            isConnected = true;
        }
        return btSocket;

    }

    public double connectAndRead()throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead = -1;
        String message;
        double weight = -1.0;

        if(!isConnected)
            mBtSocket = setupServer();
        if(mBtSocket != null) {
            mInputStream = mBtSocket.getInputStream();
            bytesRead = mInputStream.read(buffer);
            if (bytesRead != -1) {
                message = new String(buffer, 0, bytesRead);
                weight = Double.parseDouble(message);
            }
        }
        return weight;
    }

    public double getBTData() throws IOException {


        double weight = -1.0;



        weight = connectAndRead();

        return weight;

    }



    /*********************endbluetooth*****************************************/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eat);

        SensePlateDBHelper dbHelper = new SensePlateDBHelper(this);

        final AutoCompleteTextView txtFoodName = (AutoCompleteTextView) findViewById(R.id.txtNameFood);

        ArrayList<String> list = dbHelper.getFoodSuggestions();

        String[] suggestions = new String[list.size()];
        for(int i = 0; i < list.size();  i++)
            suggestions[i] = list.get(i);



        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, suggestions);
        txtFoodName.setAdapter(adapter);

        dbHelper.close();

        setupBluetooth();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_eat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public ArrayList<String> getFoodList(){

        ArrayList<String> foods = new ArrayList<>();
        final ListView listView = (ListView) findViewById(R.id.lvFoods);
        SensePlateArrayAdapter adapter = (SensePlateArrayAdapter) listView.getAdapter();
        if((adapter == null) || (adapter.getCount() == 0)){
            return new ArrayList<>();
        }else{
            int i;
            for(i = 0; i < adapter.getCount(); i++){

                foods.add(adapter.getName(i));

            }

        }

        return foods;

    }

    public ArrayList<Double> getWeightList(){

        ArrayList<Double> weights = new ArrayList<>();
        final ListView listView = (ListView) findViewById(R.id.lvFoods);
        SensePlateArrayAdapter adapter = (SensePlateArrayAdapter) listView.getAdapter();
        if((adapter == null) || (adapter.getCount() == 0)){
            return new ArrayList<>();
        }else{
            int i;
            for(i = 0; i < adapter.getCount(); i++){

                weights.add(adapter.getWeight(i));

            }

        }

        return weights;

    }

    public void refreshList(ArrayList<String> foods, ArrayList<Double> weights){

        final ListView listView = (ListView) findViewById(R.id.lvFoods);


        SensePlateArrayAdapter adapter = new SensePlateArrayAdapter(this, foods, weights);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parentAdapter, View view, int position, long id) {

                String foodName = (String) parentAdapter.getItemAtPosition(position);
                TextView txtDescription = (TextView) findViewById(R.id.txtDescriptionFood);
                TextView txtDetails = (TextView) findViewById(R.id.txtFoodDetails);
                SensePlateDBHelper dbHelper = new SensePlateDBHelper(getApplicationContext());
                List<Food> food = dbHelper.getAllFood();
                dbHelper.close();
                Food selectedFood;
                for(Food f : food){
                    if(f.getName().equals(foodName)){
                        selectedFood = f;
                        txtDetails.setText("Name: "+selectedFood.getName()+"\nCategory: "+selectedFood.getCategory()+
                            "\nCalories: "+selectedFood.getCalories()+ "g\nFat: "+selectedFood.getFat()+"g");
                    }
                }
                txtDescription.setText(foodName);
                ImageView iv = (ImageView) findViewById(R.id.imgFoodExample);
                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                iv.setImageResource(getFoodImageId(foodName));


            }

        });
    }

    private int getFoodImageId(String foodName){

        int code;
        String image_name = null;

        SensePlateDBHelper dbHelper = new SensePlateDBHelper(this);
        List<Food> foods = dbHelper.getAllFood();
        for(Food f : foods){
            if(f.getName().equals(foodName))
                image_name = f.getImage_name();
        }


        code = getResources().getIdentifier(image_name, "drawable", getPackageName());

        return code;
    }


    public void addToList(View view) {

        final AutoCompleteTextView txtFoodName = (AutoCompleteTextView) findViewById(R.id.txtNameFood);
        ArrayList<String> foods = getFoodList();
        ArrayList<Double> weights = getWeightList();
        foods.add(txtFoodName.getText().toString());

        try {
            weights.add(getBTData());
            makeToast("Passed");
        } catch (IOException e) {
            makeToast("Message error.");
        }
        refreshList(foods, weights);

        txtFoodName.setText(null);
    }



    public void removeFood(View view){

        final TextView descFood = (TextView) findViewById(R.id.txtDescriptionFood);

        String selectedFood = descFood.getText().toString();

        ArrayList<Double> weights = getWeightList();
        ArrayList<String> foods = getFoodList();
        if(foods.size() > 0){
            if(foods.contains(selectedFood)) {
                weights.remove(foods.indexOf(selectedFood));
                foods.remove(selectedFood);
            }
            descFood.setText(null);
        }
        ImageView iv = (ImageView) findViewById(R.id.imgFoodExample);
        iv.setImageDrawable(null);

        refreshList(foods, weights);

    }


    public void createMeal(View view){

        try {
            mBtSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bundle bundle = new Bundle();
        ArrayList<String> foods = getFoodList();
        ArrayList<Double> weights = getWeightList();
        double[] w = new double[weights.size()];
        for(int i = 0; i < weights.size(); i++){
            w[i] = weights.get(i);
        }


        bundle.putStringArrayList("FOOD_NAMES", foods);
        bundle.putDoubleArray("FOOD_WEIGHTS", w);
        CreateMealDialog mealDialog = new CreateMealDialog();
        mealDialog.setArguments(bundle);
        mealDialog.show(getFragmentManager(),"Create Meal");

    }

    @Override
    public void onDialogPositiveClick(android.app.DialogFragment dialog) {
        this.finish();
    }

    @Override
    public void onDialogNegativeClick(android.app.DialogFragment dialog) {

    }

    private class ShowDialog extends Thread{

        private final ProgressDialog dialog;

        public ShowDialog(Context context){

            dialog = new ProgressDialog(context);

        }

        @Override
        public void run() {


            dialog.setMessage("Getting data from the Bluetooth Adapter");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setIndeterminate(true);
            dialog.show();

        }

        public void cancel(){

            dialog.dismiss();
            try {
                this.finalize();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

        }
    }

}
