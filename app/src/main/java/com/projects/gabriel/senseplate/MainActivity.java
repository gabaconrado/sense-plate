package com.projects.gabriel.senseplate;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.projects.gabriel.senseplate.database.Food;
import com.projects.gabriel.senseplate.database.SensePlateDBHelper;


public class MainActivity extends ActionBarActivity {




    public void createDatabase(View v){

        SensePlateDBHelper dbHelper = new SensePlateDBHelper(this);
        dbHelper.populateDB(this);
        dbHelper.close();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);



        final ListView lv = (ListView) findViewById(R.id.lvMenu);
        String[] menu_items = {"Eat", "Check", "Set", "About"};

        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, menu_items);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent;
                switch(position){
                    case 0:
                        intent = new Intent(getApplicationContext(), Eat.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(getApplicationContext(), Check.class);
                        startActivity(intent);
                        break;
                    case 2:
                        break;
                    case 3:
                        intent = new Intent(getApplicationContext(), TestDB.class);
                        startActivity(intent);
                        break;
                    default:
                        break;
                }

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
