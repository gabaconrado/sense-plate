package com.projects.gabriel.senseplate;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.projects.gabriel.senseplate.database.Food;
import com.projects.gabriel.senseplate.database.Food_Meal;
import com.projects.gabriel.senseplate.database.Meal;
import com.projects.gabriel.senseplate.database.SensePlateDBHelper;

import java.util.List;


public class TestDB extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test_db);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showFood(View v){

        SensePlateDBHelper dbHelper = new SensePlateDBHelper(this);

        final TextView tv = (TextView) findViewById(R.id.textView);

        String output = new String();

        List<Food> foods = dbHelper.getAllFood();

        for(Food f : foods){

            output+="Food ID = " + f.getIdFood() + "\nFood Name = " + f.getName() + "\nFood Fats = " + f.getFat() + "\nFood Category = " + f.getCategory() +
                    "\nFood Calories = " + f.getCalories() + "\nImage Name = " + f.getImage_name() + "\n\n";

        }

        tv.setText(output);
        dbHelper.close();

    }

    public void showMeal(View v){

        SensePlateDBHelper dbHelper = new SensePlateDBHelper(this);

        final TextView tv = (TextView) findViewById(R.id.textView);

        String output = new String();

        List<Meal> meals = dbHelper.getAllMeal();

        for(Meal m : meals){

            output+="Meal ID = " + m.getIdMeal() + "\nMeal Description = " + m.getDescription() + "\nMeal Date = " +
                    m.getDate() +  "\n\n";

        }

        tv.setText(output);
        dbHelper.close();

    }

    public void showFood_Meal(View v){

        SensePlateDBHelper dbHelper = new SensePlateDBHelper(this);

        final TextView tv = (TextView) findViewById(R.id.textView);

        String output = new String();

        List<Food_Meal> food_meals = dbHelper.getAllFood_Meal();

        for(Food_Meal fm : food_meals){

            output+="Meal ID = " + fm.getIdMeal() + "\nFood_Id = " + fm.getIdFood() + "\nFood Weight = " +
                    fm.getFoodWeight() +  "\n\n";

        }

        tv.setText(output);
        dbHelper.close();

    }

}
