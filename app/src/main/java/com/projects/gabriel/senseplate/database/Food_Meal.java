package com.projects.gabriel.senseplate.database;

/**
 * Created by Gabriel on 16/03/2015.
 */
public class Food_Meal {

    private int idMeal;
    private int idFood;
    private double foodWeight;

    public int getIdFood() {
        return idFood;
    }

    public void setIdFood(int idFood) {
        this.idFood = idFood;
    }

    public int getIdMeal() {
        return idMeal;
    }

    public void setIdMeal(int idMeal) {
        this.idMeal = idMeal;
    }

    public double getFoodWeight() {
        return foodWeight;
    }

    public void setFoodWeight(double foodWeight) {
        this.foodWeight = foodWeight;
    }





}
