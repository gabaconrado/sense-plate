package com.projects.gabriel.senseplate.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.projects.gabriel.senseplate.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class SensePlateDBHelper extends SQLiteOpenHelper {

    //database version
    private static final int DATABASE_VERSION = 2;

    //database name
    private static final String DATABASE_NAME = "sensePlateDB";

    //tables names
    private static final String tableFood = "Food";
    private static final String tableMeal = "Meal";
    private static final String tableUser = "User";
    private static final String tableFood_Meal = "Food_Meal";

    //common column names
    private static final String Key_ID = "id";

    //food columns
    private static final String Key_NameFood = "name";
    private static final String Key_ImageName = "image_name";
    private static final String Key_CategoryFood = "category";
    private static final String Key_CaloriesFood = "calories";
    private static final String Key_FatsFood = "fat";

    //user columns
    private static final String Key_NameUser = "name";
    private static final String Key_AgeUser = "age";
    private static final String Key_WeightUser = "weight";
    private static final String Key_HeightUser = "height";

    //meal columns
    private static final String Key_DescriptionMeal = "description";
    private static final String Key_DateMeal = "date";

    //food_meal columns
    private static final String Key_IDMeal = "id_meal";
    private static final String Key_IDFood = "id_food";
    private static final String Key_WeightFood = "weight_food";

    //create table statements
    private static final String CREATE_TABLE_FOOD = "CREATE TABLE " + tableFood + " ( " +
            Key_ID  + " INTEGER PRIMARY KEY, " + Key_NameFood + " TEXT, " + Key_CategoryFood + " INTEGER, " +
            Key_CaloriesFood + " REAL, " + Key_FatsFood + " REAL, " + Key_ImageName + " TEXT )";

    private static final String CREATE_TABLE_USER = "CREATE TABLE " + tableUser + " ( " +
            Key_ID  + " INTEGER PRIMARY KEY, " + Key_NameUser + " TEXT, " + Key_AgeUser + " INTEGER, " +
            Key_WeightUser + " REAL, " + Key_HeightUser + " REAL )";

    private static final String CREATE_TABLE_MEAL = "CREATE TABLE " + tableMeal + " ( " +
            Key_ID  + " INTEGER PRIMARY KEY, " + Key_DescriptionMeal + " TEXT, " + Key_DateMeal + " TEXT )";

    private static final String CREATE_TABLE_FOOD_MEAL = "CREATE TABLE " + tableFood_Meal + " ( " +
            Key_ID + " INTEGER PRIMARY KEY, " + Key_IDMeal + " INTEGER, " + Key_IDFood + " INTEGER, " +
            Key_WeightFood + " REAL )";


    public SensePlateDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        db.execSQL(CREATE_TABLE_FOOD);
        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_TABLE_MEAL);
        db.execSQL(CREATE_TABLE_FOOD_MEAL);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

        db.execSQL("DROP TABLE IF EXISTS " + tableFood);
        db.execSQL("DROP TABLE IF EXISTS " + tableUser);
        db.execSQL("DROP TABLE IF EXISTS " + tableMeal);
        db.execSQL("DROP TABLE IF EXISTS " + tableFood_Meal);

        onCreate(db);
    }

    public long createFood(Food food){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Key_NameFood, food.getName());
        values.put(Key_CaloriesFood, food.getCalories());
        values.put(Key_FatsFood, food.getFat());
        values.put(Key_CategoryFood, food.getCategory());
        values.put(Key_ImageName, food.getImage_name());

        long foodID = db.insert(tableFood, null, values);

        db.close();

        return foodID;

    }

    public long createMeal(Meal meal){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();


        values.put(Key_DescriptionMeal, meal.getDescription());
        values.put(Key_DateMeal, meal.getNow());

        long mealID = db.insert(tableMeal, null, values);

        db.close();

        return mealID;

    }

    public List<Food_Meal> getAllFood_Meal(){

        List<Food_Meal> food_meal = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + tableFood_Meal;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null)
            c.moveToFirst();

        do{
            Food_Meal food = new Food_Meal();
            food.setIdFood(c.getInt(c.getColumnIndex(Key_IDFood)));
            food.setIdMeal(c.getInt(c.getColumnIndex(Key_IDMeal)));
            food_meal.add(food);
        }while(c.moveToNext());

        return food_meal;

    }

    public List<Meal> getAllMeal(){

        List<Meal> meals = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + tableMeal;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null)
            c.moveToFirst();

        do{
            Meal meal = new Meal();
            meal.setIdMeal(c.getInt(c.getColumnIndex(Key_ID)));
            meal.setDescription(c.getString(c.getColumnIndex(Key_DescriptionMeal)));
            meal.setDate(c.getString(c.getColumnIndex(Key_DateMeal)));
            meals.add(meal);
        }while(c.moveToNext());

        return meals;

    }

    public List<Meal> getAllMealWeek(String date){

        List<Meal> meals = new ArrayList<>();

        int n = getWeekDay();

        for(int i = 0; i < n; i++){

            meals.addAll(getAllMeal(dayBefore(date)));

        }


        return meals;

    }

    public List<Meal> getAllMeal(String date){

        List<Meal> meals = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + tableMeal + " WHERE " + Key_DateMeal + " LIKE '" + date + "%'";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null)
            c.moveToFirst();

        do{
            Meal meal = new Meal();
            meal.setIdMeal(c.getInt(c.getColumnIndex(Key_ID)));
            meal.setDescription(c.getString(c.getColumnIndex(Key_DescriptionMeal)));
            meal.setDate(c.getString(c.getColumnIndex(Key_DateMeal)));
            meals.add(meal);
        }while(c.moveToNext());

        return meals;

    }

    public List<Food_Meal> food_mealFromMeal(Meal m){

        List<Food_Meal> food_meals = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + tableFood_Meal + " WHERE " + Key_IDMeal + " = " + m.getIdMeal();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null)
            c.moveToFirst();

        do{
            Food_Meal food_meal = new Food_Meal();
            food_meal.setIdMeal(c.getInt(c.getColumnIndex(Key_IDMeal)));
            food_meal.setIdFood(c.getInt(c.getColumnIndex(Key_IDFood)));
            food_meal.setFoodWeight(c.getDouble(c.getColumnIndex(Key_WeightFood)));
            food_meals.add(food_meal);
        }while(c.moveToNext());

        c.close();

        return food_meals;

    }

    public List<User> getAllUser(){

        List<User> users = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + tableUser;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null)
            c.moveToFirst();

        do{
            User user = new User();
            user.setIdUser(c.getInt(c.getColumnIndex(Key_ID)));
            user.setAge(c.getInt(c.getColumnIndex(Key_AgeUser)));
            user.setName(c.getString(c.getColumnIndex(Key_NameUser)));
            user.setHeight(c.getDouble(c.getColumnIndex(Key_HeightUser)));
            user.setWeight(c.getDouble(c.getColumnIndex(Key_WeightUser)));
            users.add(user);
        }while(c.moveToNext());

        c.close();
        db.close();
        return users;

    }

    public List<Food> getAllFood(){

        List<Food> foods = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + tableFood;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null)
            c.moveToFirst();

        do{
            Food food = new Food();
            food.setIdFood(c.getInt(c.getColumnIndex(Key_ID)));
            food.setName(c.getString(c.getColumnIndex(Key_NameFood)));
            food.setCalories(c.getDouble(c.getColumnIndex(Key_CaloriesFood)));
            food.setFat(c.getDouble(c.getColumnIndex(Key_FatsFood)));
            food.setCategory(c.getInt(c.getColumnIndex(Key_CategoryFood)));
            food.setImage_name(c.getString(c.getColumnIndex(Key_ImageName)));
            foods.add(food);
        }while(c.moveToNext());

        c.close();
        db.close();
        return foods;
    }

    public ArrayList<String> getFoodSuggestions(){

        ArrayList<String> foods = new ArrayList<>();
        String selectQuery = "SELECT " + Key_NameFood + " FROM " + tableFood;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null)
            c.moveToFirst();

        do{
            foods.add(c.getString(c.getColumnIndex(Key_NameFood)));
        }while(c.moveToNext());

        c.close();
        db.close();
        return foods;
    }

    public void populateDB(Context context){

        int size = 6;

        String[] food_name = new String[size];
        String[] food_cat = new String[size];
        String[] food_cal = new String[size];
        String[] food_fat = new String[size];
        String[] food_image_names = new String[size];

        food_name =  context.getResources().getStringArray(R.array.food_names);
        food_cat = context.getResources().getStringArray(R.array.food_categories);
        food_cal = context.getResources().getStringArray(R.array.food_calories);
        food_fat = context.getResources().getStringArray(R.array.food_fat);
        food_image_names = context.getResources().getStringArray(R.array.food_image_names);

        for(int i = 0; i<size; i++){

            Food f = new Food();
            f.setName(food_name[i]);
            f.setCategory(Integer.parseInt(food_cat[i]));
            f.setCalories(Double.parseDouble(food_cal[i]));
            f.setFat(Double.parseDouble(food_fat[i]));
            f.setImage_name(food_image_names[i]);

            long id = createFood(f);

            //System.out.println(food_name[i] + " added to the database with ID " + id);

        }

    }

    public int[] idsByName(List<String> foods){

        int[] ids = new int[foods.size()];

        int i=0;
        String selectQuery = "SELECT " + Key_ID + ", " + Key_NameFood + " FROM " + tableFood;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery, null);

        if(c != null)
            c.moveToFirst();
        do {
            String name = c.getString(c.getColumnIndex(Key_NameFood));
            if(foods.contains(name)) {
                ids[i] = c.getInt(c.getColumnIndex(Key_ID));
                i++;
            }
        }while(c.moveToNext());
        c.close();
        db.close();
        return ids;
    }


    public long createFood_Meal(int idF, int idM, double weight){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();


        values.put(Key_IDFood, idF);
        values.put(Key_IDMeal, idM);
        values.put(Key_WeightFood, weight);

        long foodMealID = db.insert(tableFood_Meal, null, values);

        db.close();

        return foodMealID;
    }

    public void createMultipleFood_Meal(int idMeal, List<String> foods, double[] weights){


        int[] ids = idsByName(foods);

        for(int i = 0; i < foods.size(); i++) {
            createFood_Meal(ids[i], idMeal, weights[i]);
        }

    }

    public int getWeekDay(){

        int n = 0;

        GregorianCalendar gc = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat();
        try{
            gc.setTime((sdf.parse("01/01/yyyy")));
            switch (gc.get(Calendar.DAY_OF_WEEK)){
                case Calendar.SUNDAY:
                    n = 1;
                    break;
                case Calendar.MONDAY:
                    n = 2;
                    break;
                case Calendar.TUESDAY:
                    n = 3;
                    break;
                case Calendar.WEDNESDAY:
                    n = 4;
                    break;
                case Calendar.THURSDAY:
                    n = 5;
                    break;
                case Calendar.FRIDAY:
                    n = 6;
                    break;
                case Calendar.SATURDAY:
                    n = 7;
                    break;
                default:
                    break;
            }
        }catch(ParseException e){
            e.printStackTrace();
        }


        return n;

    }

    public String dayBefore(String date){

        int day = Integer.parseInt(date.substring(8, 10));
        int month = Integer.parseInt(date.substring(5, 7));
        int year = Integer.parseInt(date.substring(0, 4));

        day--;
        if(day == 0) {
            day = 31;
            month--;
            if(month == 0) {
                month = 12;
                year--;
            }
        }
        return(year+"-"+month+"-"+day);
    }


}
