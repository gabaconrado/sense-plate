package com.projects.gabriel.senseplate.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.projects.gabriel.senseplate.R;
import com.projects.gabriel.senseplate.database.Meal;
import com.projects.gabriel.senseplate.database.SensePlateDBHelper;

import java.util.List;


/**
 * Created by Gabriel on 17/03/2015.
 */
public class CreateMealDialog extends DialogFragment{


    public interface MealDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    MealDialogListener mListener;

    private List<String> foods;
    private double[] weights;


    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (MealDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement MealDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Build the dialog and set up the button click handlers

        Bundle bundle = getArguments();
        foods = bundle.getStringArrayList("FOOD_NAMES");
        weights = bundle.getDoubleArray("FOOD_WEIGHTS");

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View final_view = inflater.inflate(R.layout.dialog_create_meal, null);
        builder.setView(final_view)
            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Meal meal = new Meal();
                    final EditText descMeal = (EditText) final_view.findViewById(R.id.txtMealDescriptionDialog);
                    if (descMeal != null)
                        meal.setDescription(descMeal.getText().toString());
                    else {
                        Toast toast = Toast.makeText(getActivity(), "Could not find the TextView", Toast.LENGTH_LONG);
                        toast.show();
                        CreateMealDialog.this.getDialog().cancel();
                    }
                    SensePlateDBHelper dbHelper = new SensePlateDBHelper(getActivity());
                    int idMeal = (int) dbHelper.createMeal(meal);
                    dbHelper.createMultipleFood_Meal(idMeal, foods, weights);
                    dbHelper.close();
                    Toast toast = Toast.makeText(getActivity(), "Meal Successfully created.", Toast.LENGTH_LONG);
                    toast.show();

                }
            })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        CreateMealDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

}
