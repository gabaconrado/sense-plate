package com.projects.gabriel.senseplate.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.projects.gabriel.senseplate.R;

/**
 * Created by Gabriel on 25/03/2015.
 */
public class FoodDetailsDialog extends DialogFragment {


    private TextView txtFoodName, txtFoodCategory, txtFoodFat, txtFoodCalories;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Build the dialog and set up the button click handlers

        Bundle bundle = getArguments();
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View final_view = inflater.inflate(R.layout.dialog_food_details, null);

        



        builder.setView(final_view)
                .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });

        return builder.create();
    }

}
