package com.projects.gabriel.senseplate.util;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by Gabriel on 25/02/2015.
 */
public class FoodListArrayAdapter extends ArrayAdapter<String> {


    public FoodListArrayAdapter(Context context, int textViewResourceId, List<String> objects) {
        super(context, textViewResourceId, objects);

    }

}
