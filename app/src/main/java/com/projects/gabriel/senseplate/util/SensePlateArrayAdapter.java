package com.projects.gabriel.senseplate.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.projects.gabriel.senseplate.R;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Gabriel on 18/03/2015.
 */
public class SensePlateArrayAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final List<String> names;
    private final List<Double> weights;

    public SensePlateArrayAdapter(Context context, List<String> objects, List<Double> objects2) {
        super(context, R.layout.list_food, objects);
        this.context = context;
        this.names = objects;
        this.weights = objects2;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_food, parent, false);

        TextView tvNameFood = (TextView) rowView.findViewById(R.id.txtNameFoodList);
        TextView tvWeightFood = (TextView) rowView.findViewById(R.id.txtWeightFoodList);
        tvNameFood.setText(names.get(position));
        Double weight = weights.get(position);
        DecimalFormat df = new DecimalFormat("#.00");
        tvWeightFood.setText(String.valueOf(df.format(weight)) + "g");


        return rowView;
    }

    public Double getWeight(int position){
        return weights.get(position);
    }

    public String getName(int position){
        return names.get(position);
    }

}


